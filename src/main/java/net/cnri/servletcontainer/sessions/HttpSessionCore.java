package net.cnri.servletcontainer.sessions;

import java.util.Collection;

public interface HttpSessionCore {
    String getId();
    void setId(String id);
    long getCreationTime();
    long getLastAccessedTime();
    void setLastAccessedTime(long now);
    int getMaxInactiveInterval();
    void setMaxInactiveInterval(int interval);
    long getExpiration();
    void setExpiration(long expiration);

    boolean canInvalidate();
    boolean isValid();
    void setValid(boolean valid);

    Object getAttribute(String name);
    Collection<String> getAttributeNames();
    Object setAttribute(String name, Object value);
    Object removeAttribute(String name);

    void incrementRequestCount();
    int decrementAndGetRequestCount();
    int getRequestCount();

    default boolean handlesEvents() {
        return false;
    }

    public static boolean isExpired(HttpSessionCore sessionCore, long time) {
        long exp = sessionCore.getExpiration();
        if (exp > 0) {
            if (exp < time) {
                return true;
            }
        }
        long maxInactiveInterval = sessionCore.getMaxInactiveInterval();
        if (maxInactiveInterval > 0) {
            long lastAccessedTime = sessionCore.getLastAccessedTime();
            if (lastAccessedTime + 1000L * maxInactiveInterval < time) {
                return true;
            }
        }
        return false;
    }
}
