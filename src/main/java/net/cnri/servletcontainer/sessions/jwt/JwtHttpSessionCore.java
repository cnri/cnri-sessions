package net.cnri.servletcontainer.sessions.jwt;

import java.util.Collection;
import java.util.Map;

import net.cnri.servletcontainer.sessions.HttpSessionCore;

public class JwtHttpSessionCore implements HttpSessionCore {

    final String id;
    final long iat;
    final long exp;
    final Map<String, Object> atts;

    public JwtHttpSessionCore(String id, long iat, long exp, Map<String, Object> atts) {
        this.id = id;
        this.iat = iat;
        this.exp = exp;
        this.atts = atts;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        throw new UnsupportedOperationException();
    }

    @Override
    public long getCreationTime() {
        return iat * 1000L;
    }

    @Override
    public long getLastAccessedTime() {
        return 0;
    }

    @Override
    public void setLastAccessedTime(long now) {
        // no-op
    }

    @Override
    public int getMaxInactiveInterval() {
        return 0;
    }

    @Override
    public void setMaxInactiveInterval(int interval) {
        // no-op
    }

    @Override
    public long getExpiration() {
        return exp * 1000L;
    }

    @Override
    public void setExpiration(long expiration) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean canInvalidate() {
        return false;
    }

    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public void setValid(boolean valid) {
        // no-op
    }

    @Override
    public Object getAttribute(String name) {
        return atts.get(name);
    }

    @Override
    public Collection<String> getAttributeNames() {
        return atts.keySet();
    }

    @Override
    public Object setAttribute(String name, Object value) {
        if (value.equals(atts.get(name))) return value;
        throw new UnsupportedOperationException();
    }

    @Override
    public Object removeAttribute(String name) {
        // called by clean-up, so no error, even if no removal
        return atts.get(name);
    }

    @Override
    public void incrementRequestCount() {
        // no-op
    }

    @Override
    public int decrementAndGetRequestCount() {
        return 0;
    }

    @Override
    public int getRequestCount() {
        return 0;
    }

}
