package net.cnri.servletcontainer.sessions;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import java.util.Collections;
import java.util.Enumeration;

public class HttpSessionImpl implements HttpSession {

    private final HttpSessionManager sessionManager;
    private final HttpSessionCore sessionCore;
    private final long lastAccessedTime;
    private final boolean isNew;

    public HttpSessionImpl(HttpSessionManager sessionManager, HttpSessionCore sessionCore, boolean isNew) {
        this.sessionManager = sessionManager;
        this.sessionCore = sessionCore;
        this.isNew = isNew;
        this.lastAccessedTime = sessionCore.getLastAccessedTime();
    }

    public HttpSessionCore getSessionCore() {
        return sessionCore;
    }

    @Override
    public long getCreationTime() {
        checkValid();
        return sessionCore.getCreationTime();
    }

    @Override
    public String getId() {
        return sessionCore.getId();
    }

    @Override
    public long getLastAccessedTime() {
        checkValid();
        return lastAccessedTime;
    }

    @Override
    public ServletContext getServletContext() {
        return sessionManager.getServletContext();
    }

    @Override
    public void setMaxInactiveInterval(int interval) {
        sessionCore.setMaxInactiveInterval(interval);
    }

    @Override
    public int getMaxInactiveInterval() {
        return sessionCore.getMaxInactiveInterval();
    }

    @Override
    @Deprecated
    public javax.servlet.http.HttpSessionContext getSessionContext() {
        checkValid();
        return nullSessionContext;
    }

    @Override
    public void invalidate() {
        if (!sessionCore.canInvalidate()) {
            throw new UnsupportedOperationException();
        }
        checkValid();
        sessionManager.ensureInvalid(sessionCore);
    }

    void ensureInvalid() {
        sessionCore.setValid(false);
        if (!sessionCore.handlesEvents()) {
            while (true) {
                boolean found = false;
                for (String name : sessionCore.getAttributeNames()) {
                    Object priorValue = sessionCore.removeAttribute(name);
                    notifyUnbound(name, priorValue);
                }
                if (!found) break;
            }
        }
    }

    @Override
    public boolean isNew() {
        checkValid();
        return isNew;
    }

    @Override
    public Object getAttribute(String name) {
        checkValid();
        return sessionCore.getAttribute(name);
    }

    @Override
    @Deprecated
    public Object getValue(String name) {
        return getAttribute(name);
    }

    @Override
    public Enumeration<String> getAttributeNames() {
        checkValid();
        return Collections.enumeration(sessionCore.getAttributeNames());
    }

    @Override
    @Deprecated
    public String[] getValueNames() {
        checkValid();
        return sessionCore.getAttributeNames().toArray(new String[0]);
    }

    @Override
    public void setAttribute(String name, Object value) {
        if (value == null) {
            removeAttribute(name);
            return;
        }
        checkValid();
        Object priorValue = sessionCore.setAttribute(name, value);
        if (value == priorValue) return;
        if (!sessionCore.handlesEvents()) {
            notifyUnbound(name, priorValue);
            notifyBound(name, value);
        }
    }

    @Override
    @Deprecated
    public void putValue(String name, Object value) {
        setAttribute(name, value);
    }

    @Override
    public void removeAttribute(String name) {
        checkValid();
        Object priorValue = sessionCore.removeAttribute(name);
        if (!sessionCore.handlesEvents()) {
            notifyUnbound(name, priorValue);
        }
    }

    @Override
    @Deprecated
    public void removeValue(String name) {
        removeAttribute(name);
    }

    private void notifyUnbound(String name, Object priorValue) {
        if (priorValue instanceof HttpSessionBindingListener) {
            ((HttpSessionBindingListener) priorValue).valueUnbound(new HttpSessionBindingEvent(this, name, priorValue));
        }
    }

    private void notifyBound(String name, Object value) {
        if (value instanceof HttpSessionBindingListener) {
            ((HttpSessionBindingListener) value).valueBound(new HttpSessionBindingEvent(this, name, value));
        }
    }

    private void checkValid() {
        if (!sessionCore.isValid()) throw new IllegalStateException("session " + sessionCore.getId() + " not valid");
    }

    public boolean isValid() {
        return sessionCore.isValid();
    }

    @Deprecated
    private static final javax.servlet.http.HttpSessionContext nullSessionContext = new javax.servlet.http.HttpSessionContext() {
        @Deprecated
        @Override
        public HttpSession getSession(String sessionId) {
            return null;
        }

        @Deprecated
        @Override
        public Enumeration<String> getIds() {
            return Collections.enumeration(Collections.emptyList());
        }
    };
}

