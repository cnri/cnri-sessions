package net.cnri.servletcontainer.sessions;

import java.util.Map;
import java.util.stream.Stream;

import javax.servlet.http.HttpServletRequest;

public interface HttpSessionCoreStore {
    void init(HttpSessionManager sessionManager, String sessionCoreStoreInit);
    void clear();
    void destroy();
    HttpSessionCore getSession(String id);
    HttpSessionCore createSession(Map<String, Object> attributes);
    void changeSessionId(HttpSessionCore session, String newId);
    void removeSession(String id);
    Stream<HttpSessionCore> getSessionsByKeyValue(String key, String value);

    @SuppressWarnings("unused")
    default HttpSessionCore getSession(HttpServletRequest req, String id) {
        return getSession(id);
    }
    @SuppressWarnings("unused")
    default HttpSessionCore createSession(HttpServletRequest req, Map<String, Object> attributes) {
        return createSession(attributes);
    }
    @SuppressWarnings("unused")
    default void changeSessionId(HttpServletRequest req, HttpSessionCore session, String newId) {
        changeSessionId(session, newId);
    }

}
